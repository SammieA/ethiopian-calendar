package com.example.ethiopiancalendar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.MutableDateTime;
import org.joda.time.chrono.EthiopicChronology;

import java.util.ArrayList;

public class EthiopicCalendarView extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";
    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";
    // how many days to show, defaults to six weeks, 42 days
    private static int DAYS_COUNT = 42;
    String[] ethiopianDays = new String[]{"እሁድ", "ሰኞ", "ማክሰኞ", "ረቡዕ", "ሐሙስ", "ዓርብ", "ቅዳሜ"};
    // current displayed month
    String[] ethiopianMonths = new String[]{"መስከረም", "ጥቅምት", "ኅዳር", "ታኅሣሥ", "ጥር", "የካቲት", "መጋቢት", "ሚያዝያ", "ግንቦት", "ሰኔ", "ሐምሌ", "ነሐሴ", "ጳጉሜ"};
    //    ******************************/
    DateTimeZone timeZone = DateTimeZone.forOffsetHours(3);
    Chronology ethiopicChronology = EthiopicChronology.getInstance(timeZone);
    //    ******************************/
    private final DateTime currentDate = DateTime.now(ethiopicChronology);
    DateTime dateTime = new DateTime(ethiopicChronology);
    // date format
    private String dateFormat;
    //event handling
    private EventHandler eventHandler = null;
    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    public EthiopicCalendarView(Context context) {
        super(context);
    }

    public EthiopicCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public EthiopicCalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = findViewById(R.id.calendar_header);
        btnPrev = findViewById(R.id.calendar_prev_button);
        btnNext = findViewById(R.id.calendar_next_button);
        txtDate = findViewById(R.id.calendar_date_display);
        grid = findViewById(R.id.calendar_grid);

    }

    private void assignClickHandlers() {
        // add a month and refresh UI
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTime = dateTime.plusMonths(1);
                updateCalendar();
            }
        });

        // subtract a month and refresh UI
        btnPrev.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTime = dateTime.minusMonths(1);
                updateCalendar();
            }
        });

        // clicking a day
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                eventHandler.onDayClick((DateTime) parent.getItemAtPosition(position));
            }
        });

    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        ArrayList<DateTime> cells = new ArrayList<>();

        MutableDateTime mutableDT = dateTime.toMutableDateTime();
        mutableDT.setChronology(ethiopicChronology);

        mutableDT.setDayOfMonth(1);
        int monthBeginningCell = mutableDT.getDayOfWeek() - 1;

        mutableDT.addDays(-monthBeginningCell);

        while (cells.size() < DAYS_COUNT) {
            DateTime dt = mutableDT.toDateTime(ethiopicChronology);
            cells.add(dt);
            mutableDT.addDays(1);
        }

        // update grid
        grid.setAdapter(new CalendarAdapter(getContext(), cells));

        // update title
        txtDate.setText(String.format("%s %d", ethiopianMonths[dateTime.getMonthOfYear() - 1], dateTime.getYear()));
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface EventHandler {
        void onDayClick(DateTime date);
    }

    private class CalendarAdapter extends ArrayAdapter<DateTime> {

        // for view inflation
        private final LayoutInflater inflater;

        public CalendarAdapter(Context context, ArrayList<DateTime> days) {
            super(context, R.layout.control_calendar_day, days);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            // day in question
            DateTime date = getItem(position);
            int day = date.getDayOfMonth();
            int month = date.getMonthOfYear();
            int year = date.getYear();

            // today
            DateTime today = new DateTime().withChronology(EthiopicChronology.getInstance());

            // inflate item if it does not exist yet
            if (view == null)
                view = inflater.inflate(R.layout.control_calendar_day, parent, false);

            // if this day has an event, specify event image
            view.setBackgroundResource(0);

            // clear styling
            ((TextView) view).setTypeface(null, Typeface.NORMAL);
            ((TextView) view).setTextColor(Color.BLACK);


            if (month != today.getMonthOfYear() || year != today.getYear()) {
                // if this day is outside current month, grey it out
                ((TextView) view).setTextColor(getResources().getColor(R.color.greyed_out));
            } else if (day == today.getDayOfMonth()) {
                // if it is today, set it to blue/bold
                ((TextView) view).setTypeface(null, Typeface.BOLD);
                ((TextView) view).setTextColor(getResources().getColor(R.color.today));
                ((TextView) view).setBackgroundResource(R.drawable.rectangle_stroke);
            }

            // set text
            ((TextView) view).setText(String.valueOf(day));

            return view;
        }
    }
}
