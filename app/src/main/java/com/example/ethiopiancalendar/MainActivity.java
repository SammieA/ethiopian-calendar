package com.example.ethiopiancalendar;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.joda.time.DateTime;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EthiopicCalendarView cv = findViewById(R.id.calendar_view);

        // assign event handler
        cv.setEventHandler(new EthiopicCalendarView.EventHandler() {
            @Override
            public void onDayClick(DateTime dateTime) {
                // toast returned day
                Toast.makeText(MainActivity.this, dateTime.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}